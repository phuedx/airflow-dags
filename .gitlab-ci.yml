# Include external file for images build with kokkuri
include:
  - project: 'repos/releng/kokkuri'
    file:
      - includes/images.yaml

stages:
  - build
  - test
  - publish

workflow:
  rules:
    # Run for merge request events
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    # Run for protected tags
    - if: $CI_COMMIT_TAG && $CI_COMMIT_REF_PROTECTED

variables:

  # Debian package name
  PACKAGE_NAME: airflow

  # Airflow debian package version
  PACKAGE_VERSION: 2.6.3-py3.10-20230711

  # Path to the Dockerfile generating the deb file
  PACKAGE_DOCKERFILE: debian/Dockerfile

  # Path to the Dockerfile within the Docker image
  PACKAGE_DOCKER_FILENAME: /srv/airflow/airflow-${PACKAGE_VERSION}_amd64.deb

  # Bullseye image version
  BULLSEYE_VERSION: 20230528

lint-dockerfile:
  image: 'docker-registry.wikimedia.org/bullseye:${BULLSEYE_VERSION}'
  stage: build
  only:
    changes:
      - debian/Dockerfile
  script:
    - apt-get -qq update && apt-get -qq install -y wget ca-certificates
    - wget --quiet -O /bin/hadolint https://github.com/hadolint/hadolint/releases/download/v2.12.0/hadolint-Linux-x86_64
    - echo "56de6d5e5ec427e17b74fa48d51271c7fc0d61244bf5c90e828aab8362d55010 /bin/hadolint" > /tmp/hadolintcheck && \
    - sha256sum --check --strict /tmp/hadolintcheck && \
    - chmod +x /bin/hadolint
    - hadolint --config debian/hadolint.yml --failure-threshold warning debian/Dockerfile

.build-image-conda_env:
  extends: .kokkuri:build-and-publish-image
  stage: build
  rules:
    - when: manual
      allow_failure: true
  variables:
    BUILD_CONFIG: debian/Dockerfile
    BUILD_TARGET_PLATFORMS: linux/amd64
    PUBLISH_IMAGE_EXTRA_TAGS: 'ci'

build-image-conda_env_test:
  extends: .build-image-conda_env
  variables:
    BUILD_VARIANT: conda_env_test
    PUBLISH_IMAGE_TAG: "${PACKAGE_VERSION}-test"

build-image-conda_env_lint:
  extends: .build-image-conda_env
  variables:
    BUILD_VARIANT: conda_env_lint
    PUBLISH_IMAGE_TAG: "${PACKAGE_VERSION}-lint"

.activate-conda-script:
  - source /opt/miniconda/bin/activate
  - conda activate airflow

# Note: The test image used for pytest is missing the Pyspark jars which are not used in a unit test environment.
pytest:
  image: docker-registry.wikimedia.org/repos/data-engineering/airflow-dags:${PACKAGE_VERSION}-test
  script:
    - !reference [.activate-conda-script]
    - >
      PYTHONPATH=.:wmf_airflow_common/plugins
      COVERAGE_FILE=/tmp/.coverage
      pytest
      --runslow
      --cov=wmf_airflow_common
      --cov analytics_test
      --cov-report=term
      --cov-report=xml:coverage.xml
      -svv
      --junitxml=junit_pytest_report.xml
      -o cache_dir=/tmp/pytest_cache_dir &&
      ls -lh
  # Match coverage total from job log output.
  # See: https://docs.gitlab.com/ee/ci/yaml/index.html#coverage
  coverage: '/^TOTAL.+?(\d+\%)$/'
  artifacts:
    when: always
    reports:
      # Publish coverage.xml as an artifact, so it can be used in the GitLab CI UI.
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
      # Publish pytest report in junit xml format, so it can be used in the GitLab CI UI.
      junit: junit_pytest_report.xml

.linting-base:
  image: docker-registry.wikimedia.org/repos/data-engineering/airflow-dags:${PACKAGE_VERSION}-lint
  stage: test

flake8:
  extends: .linting-base
  script:
    - !reference [.activate-conda-script]
    - flake8

mypy:
  extends: .linting-base
  script:
    - !reference [.activate-conda-script]
    - mypy

black:
  extends: .linting-base
  script:
    - !reference [.activate-conda-script]
    - black --check .

isort:
  extends: .linting-base
  script:
    - !reference [.activate-conda-script]
    - isort --check .

publish-debian-package:
  # It's actually just a build image.
  extends: .kokkuri:build-and-run-image
  stage: publish
  variables:
    BUILD_CONFIG: debian/Dockerfile
    BUILD_TARGET_PLATFORMS: linux/amd64
    BUILD_VARIANT: publish_deb
    GENERIC_PACKAGE_REGISTRY_URI: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic"
  tags: [trusted]
  rules:
    - when: manual
      allow_failure: true
  script:
    # Overriding script to control character escaping.
    - >
      BUILDCTL_BUILD_FLAGS="--opt build-arg:GENERIC_PACKAGE_REGISTRY_URI=$GENERIC_PACKAGE_REGISTRY_URI --opt build-arg:CI_JOB_TOKEN=$CI_JOB_TOKEN" 
      kokkuri image build
