"""
### Geoeditors weekly DP edits
Aggregate editors_daily traditional buckets of geoeditors_monthly, but on a weekly basis using DP.
Note: This dataset does NOT contain bots actions and only considers edit actions.
"""

from datetime import datetime, timedelta
from mergedeep import merge

from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.providers.apache.hive.sensors.named_hive_partition import NamedHivePartitionSensor

from platform_eng.config.dag_config import default_args, artifact
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator, SparkSqlOperator

dag_id = "geoeditors_dag"
var_props = VariableProperties(f"{dag_id}_config")
current_month = '{{ data_interval_start.strftime("%Y-%m") }}'
venv = 'differential-privacy-0.1.0.conda.tgz'

modified_args = merge(
    default_args,
    {
        # Custom job settings, ~20% cluster resources
        # See https://wikitech.wikimedia.org/wiki/Analytics/Systems/Cluster/Spark#Regular_jobs for normal cluster reference
        # (24GB exec mem + 4GB exec mem overhead) * 24 instances + (10GB driver mem * 2 instances) = 692GB
        'driver_cores': 2,
        'driver_memory': '10G',
        'executor_cores': 12,
        'executor_memory': '24G',
        'conf': {
            'spark.dynamicAllocation.maxExecutors': '24',
            'spark.sql.shuffle.partitions': 288, # 24 instances * 12 cores / instance = 288 2GB partitions
            'spark.sql.sources.partitionOverwriteMode': 'dynamic',
            'spark.executor.memoryOverhead': 4096,
            'spark.sql.warehouse.dir': '/tmp',
        },
    }
)

with DAG(
    dag_id=dag_id,
    start_date=var_props.get_datetime("start_date", datetime(2023, 6, 1)),
    schedule="@monthly",
    max_active_runs=1,
    tags=["spark", "hive", "geoeditors", "differential_privacy"],
    default_args=modified_args
) as dag:
    hive_sensor = NamedHivePartitionSensor(
        task_id="wait_for_editors_daily",
        partition_names=[f"wmf.editors_daily/month={current_month}"],
        poke_interval=timedelta(hours=1).total_seconds(),
    )

    conda_env = var_props.get('conda_env', artifact(venv))
    args_weekly = [current_month, "geoeditors_weekly"]
    args_monthly = [current_month, "geoeditors_monthly"]

    do_dp_geoeditors_weekly = SparkSubmitOperator.for_virtualenv(
        task_id="do_dp_geoeditors_weekly",
        virtualenv_archive=conda_env,
        use_virtualenv_spark=True,
        entry_point='lib/python3.7/site-packages/differential_privacy/geoeditors_weekly.py',
        launcher='skein',
        application_args=args_weekly,
        env_vars={"SPARK_CONF_DIR": "/etc/spark3/conf"},
        deploy_mode='cluster'
    )

    do_dp_geoeditors_monthly = SparkSubmitOperator.for_virtualenv(
        task_id="do_dp_geoeditors_monthly",
        virtualenv_archive=conda_env,
        use_virtualenv_spark=True,
        entry_point='lib/python3.7/site-packages/differential_privacy/geoeditors_monthly.py',
        launcher='skein',
        application_args=args_monthly,
        env_vars={"SPARK_CONF_DIR": "/etc/spark3/conf"},
        deploy_mode='cluster'
    )

    tmp_directory_weekly = var_props.get('tmp_directory', f'hdfs:///tmp/geoeditors_weekly/{current_month}')
    pub_file_weekly = var_props.get('pub_file', f'hdfs:///wmf/data/published/datasets/geoeditors_weekly/{current_month}.tsv')
    tmp_directory_monthly = var_props.get('tmp_directory', f'hdfs:///tmp/geoeditors_monthly/{current_month}')
    pub_file_monthly = var_props.get('pub_file', f'hdfs:///wmf/data/published/datasets/geoeditors_monthly/{current_month}.tsv')

    publish_weekly = SparkSqlOperator(
        task_id="publish_weekly",
        sql=var_props.get(
            'hql_path',
            'https://gitlab.wikimedia.org/repos/security/differential-privacy/-/raw/main/differential_privacy/hql/publish_geoeditors_weekly.hql'
        ),
        query_parameters={
            'destination_directory': tmp_directory_weekly,
            'month': current_month,
            'coalesce_partitions': 1
        },
    )

    publish_monthly = SparkSqlOperator(
        task_id="publish_monthly",
        sql=var_props.get(
            'hql_path',
            'https://gitlab.wikimedia.org/repos/security/differential-privacy/-/raw/main/differential_privacy/hql/publish_geoeditors_monthly.hql'
        ),
        query_parameters={
            'destination_directory': tmp_directory_monthly,
            'month': current_month,
            'coalesce_partitions': 1
        },
    )

    clean_up_weekly = BashOperator(
        task_id="clean_up_weekly",
        bash_command=f"hdfs dfs -mv {tmp_directory_weekly}/*.csv {pub_file_weekly} && hdfs dfs -chmod +r {pub_file_weekly} && hdfs dfs -rm -r {tmp_directory_weekly}"
    )

    clean_up_monthly = BashOperator(
        task_id="clean_up_monthly",
        bash_command=f"hdfs dfs -mv {tmp_directory_monthly}/*.csv {pub_file_monthly} && hdfs dfs -chmod +r {pub_file_monthly} && hdfs dfs -rm -r {tmp_directory_monthly}"
    )

    clean_up_monthly << publish_monthly << do_dp_geoeditors_monthly << hive_sensor >> do_dp_geoeditors_weekly >> publish_weekly >> clean_up_weekly
