#!/usr/bin/bash

# Run airflow cli linked to dev instance.
#
# Run the airflow cli in the same environment as run_dev_instace.sh.
# TODO: Merge me later with run_dev_instance.sh
#
# Examples:
#   ./dev_instance/aiflow_cli.sh dags list
#   ./dev_instance/aiflow_cli.sh dags trigger -e '2022-02-13T15:46:38Z' aqs_hourly


# Define defaults.
airflow_home="${HOME}/airflow"
dev_env_name="airflow_development"
script_dir="$(cd -- "$(dirname "${0}")" > "/dev/null" 2>&1 ; pwd -P)"
conda_execs_script="/usr/lib/airflow/etc/profile.d/conda.sh"

export AIRFLOW_HOME="${airflow_home}"
export PYTHONPATH="${script_dir}/../"
source "$conda_execs_script"
conda activate "$dev_env_name"
airflow $@