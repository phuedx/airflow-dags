import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "dumps", "dumps_merge_visibility_events_to_wikitext_raw_dag.py"]


@pytest.fixture(name="dag")
def fixture_dag(dagbag):
    assert dagbag.import_errors == {}
    return dagbag.get_dag(dag_id="dumps_merge_visibility_events_to_wikitext_raw")


def test_mediawiki_revision_visibility_change_hourly_load(dag):
    assert dag is not None
    assert len(dag.tasks) == 2
