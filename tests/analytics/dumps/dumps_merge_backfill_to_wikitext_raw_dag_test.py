import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "dumps", "dumps_merge_backfill_to_wikitext_raw_dag.py"]


def test_mediawiki_wikitext_history_to_wikitext_raw_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag0 = dagbag.get_dag(dag_id="dumps_merge_backfill_to_wikitext_raw_enwiki")
    dag1 = dagbag.get_dag(dag_id="dumps_merge_backfill_to_wikitext_raw_wikidatawiki")
    dag2 = dagbag.get_dag(dag_id="dumps_merge_backfill_to_wikitext_raw_commonswiki")
    dag3 = dagbag.get_dag(dag_id="dumps_merge_backfill_to_wikitext_raw_all_other_wikis")
    assert dag0 is not None
    assert dag1 is not None
    assert dag2 is not None
    assert dag3 is not None

    assert len(dag0.tasks) == 25
    assert len(dag1.tasks) == 25
    assert len(dag2.tasks) == 25
    assert len(dag3.tasks) == 25
