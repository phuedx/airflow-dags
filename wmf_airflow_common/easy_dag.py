from typing import Any, Dict, List

from airflow import DAG


class EasyDAGFactory:
    """
    This class is a factory to create EasyDAGs.

    EasyDAGs are regular Airflow DAGs with default values for some fields.
    This way, when creating a DAG in a DAG file, the developer needs to pass
    less parameters that are usually common for all DAGs. For example:

        # With a regular DAG:
        # You need to pass all parameters every time,
        # and some through default_args.
        with DAG(
            dag_id="my_dag_id",
            start_date=datetime(2023, 3, 1, 0),
            schedule="@hourly",
            user_defined_filters=filters,
            default_args={
                **default_args,
                **cassandra_default_conf,
                **druid_default_conf,
                "sla": timedelta(hours=6),
                "email": "some@email.org"
            },
        ) as dag:
            ...

        # With EasyDAG:
        # You first create a factory passing all the base parameters
        # and shortcuts (do this for instance in dag_config.py).
        EasyDAG = EasyDAGFactory(
            factory_default_args={
                **default_args,
                **cassandra_default_conf,
                **druid_default_conf,
            },
            factory_default_args_shortcuts=["sla", "email"],
            factory_user_defined_filters=filters,
        ).create_easy_dag

        # And then create the EasyDAG in the DAG file.
        with EasyDAG(
            dag_id="my_dag_id",
            start_date=datetime(2023, 3, 1, 0),
            schedule="@hourly",
            sla=timedelta(hours=6),
            email="some@email.org",
        ) as dag:
            ...

    You can override the factory defaults when creating the EasyDAG, too.
    """

    def __init__(
        self,
        factory_default_args: Dict[str, Any] | None = None,
        factory_default_args_shortcuts: List[str] | None = None,
        factory_user_defined_filters: Dict[str, Any] | None = None,
    ):
        """
        Initializes an instance of EasyDAGFactory.
        :param factory_default_args:
            Airflow default args dictionary to be passed to all EasyDAGs.
            Can be overridden (merge) when creating each EasyDAG.
        :param factory_default_args_shortcuts:
            List of Airflow default args names that should be accepted
            as direct parameters when creating each EasyDAG.
        :param factory_user_defined_filters:
            Airflow filters dictionary to be passed to all EasyDAGs.
        """
        self.factory_default_args = factory_default_args or {}
        self.factory_default_args_shortcuts = factory_default_args_shortcuts or []
        self.factory_user_defined_filters = factory_user_defined_filters or {}

    def create_easy_dag(self, dag_id: str, **kwargs: Any) -> DAG:
        """
        Returns an instance of an Airflow DAG with the given dag_id and kwargs.
        The DAG's default_args and user_defined_filters will be set to the
        factory settings specified when creating the factory instance.
        They can be overridden (merge) if passed explicitly to this method.
        :param dag_id:
            The DAG id string.
        :param kwargs:
            Dictionary of keyword arguments for this method.
            Accepts the same arguments as a regular Airflow DAG,
            plus the ones specified in factory_default_args_shortcuts.
        """
        # Build the default args dictionary to pass to the DAG.
        easy_dag_default_args = self.factory_default_args
        if "default_args" in kwargs:
            easy_dag_default_args = {
                **self.factory_default_args,
                **kwargs["default_args"],
            }
            del kwargs["default_args"]
        # For each default args shortcut that is specified,
        # add it to the EasyDAG's default args.
        for arg_name in self.factory_default_args_shortcuts:
            if arg_name in kwargs:
                easy_dag_default_args[arg_name] = kwargs[arg_name]
                del kwargs[arg_name]
        # Build the filters dictionary to pass to the DAG.
        easy_user_defined_filters = self.factory_user_defined_filters
        if "user_defined_filters" in kwargs:
            easy_user_defined_filters = {
                **self.factory_user_defined_filters,
                **kwargs["user_defined_filters"],
            }
            del kwargs["user_defined_filters"]
        # Create and return the DAG with modified parameters.
        return DAG(
            dag_id,
            default_args=easy_dag_default_args,
            user_defined_filters=easy_user_defined_filters,
            **kwargs,
        )
