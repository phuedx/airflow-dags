"""
************
NOTICE: This job is work in progress. If it fails, do please ignore.
************

This job backfills data from wmf.mediawiki_wikitext_history into an
Iceberg table meant to be the base to create dumps from.

We accomplish this by running a pyspark job that runs a MERGE INTO
that updates a particular (wiki_db, revision_id) tuple if the target table watermark is
older than the source watermark.

More info about the pyspark job at:
https://gitlab.wikimedia.org/repos/data-engineering/dumps/mediawiki-content-dump/-/blob/main/mediawiki_content_dump/backfill_merge_into.py
"""
from datetime import datetime, timedelta

from analytics.config.dag_config import artifact, create_easy_dag, hadoop_name_node
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.sensors.url import URLSensor

props = DagProperties(
    # DAG settings
    start_date=datetime(2023, 7, 1),
    current_year=2023,  # unfortunately, it seems we need to set this manually to make tests happy.
    sla=timedelta(days=7),
    conda_env=artifact("mediawiki-content-dump-0.1.0.dev0-use-rev-deleted.conda.tgz"),
    # target table
    hive_wikitext_raw_table="wmf_dumps.wikitext_raw_rc1",
    # source tables
    hive_mediawiki_wikitext_history_table="wmf.mediawiki_wikitext_history",
    hive_mediawiki_revision_table="wmf_raw.mediawiki_revision",
    # Spark job tuning
    driver_memory="32G",
    driver_cores="4",
    executor_memory="20G",
    executor_cores="2",
    num_executors="105",
    spark_executor_memoryOverhead="4G",
    # avoid FetchFailed exceptions as much as possible
    spark_sql_shuffle_partitions="65536",
    spark_shuffle_io_retryWait="15s",
    spark_shuffle_io_maxRetries="15",
    spark_network_timeout="600s",
    # maxResultSize default is 1g, and its giving us problems with MERGE INTO tasks
    spark_driver_maxResultSize="8G",
    # extra settings as per https://wikitech.wikimedia.org/wiki/Data_Engineering/Systems/Cluster/Spark#Executors
    spark_shuffle_file_buffer="1m",
    spark_shuffle_service_index_cache_size="256m",
    spark_io_compression_lz4_blockSize="512KB",
    # this job runs Spark 3.3. Disable shuffle service for now as 3.1's is incompatible
    spark_shuffle_service_enabled="False",
    spark_dynamicAllocation_enabled="False",
    # Spark 3.3's adaptive partition coalescing is OOMing our job
    spark_sql_adaptive_coalescePartitions_enabled="False",
)

# the below groups split the backfilling based on the revision count of each wiki
# relative to the revisions of enwiki.
# this split should allow us to tune this job to the size of enwiki, and hopefully run all
# other groups without needing to tune them individually

groups = [
    {"name": "enwiki", "exclusive": False, "members": ["enwiki"]},  # (size is 100% of revisions of enwiki)
    {"name": "wikidatawiki", "exclusive": False, "members": ["wikidatawiki"]},  # (size is 71%  of revisions of enwiki)
    {"name": "commonswiki", "exclusive": False, "members": ["commonswiki"]},  # (size is 42%  of revisions of enwiki)
    {
        "name": "all_other_wikis",
        "exclusive": True,
        "members": ["enwiki", "wikidatawiki", "commonswiki"],
    },  # (size is 97%  of revisions of enwiki)
]

for group in groups:
    with create_easy_dag(
        dag_id=f"dumps_merge_backfill_to_wikitext_raw_{group['name']}",
        doc_md=__doc__,
        start_date=props.start_date,
        schedule="@monthly",
        tags=[
            "monthly",
            "from_hive",
            "to_iceberg",
            "requires_wmf_mediawiki_wikitext_history",
            "requires_wmf_raw_mediawiki_revision",
            "uses_spark",
            "mediawiki_dumps",
        ],
        sla=props.sla,
        max_active_runs=1,
        max_active_tasks=1,  # MERGE INTOs use lots of resources, let's limit it to 1 concurrent per group
        email="xcollazo@wikimedia.org",  # overriding alert email for now.
        # it's ok if this fails, and we don't want to alert ops week folk.
    ) as dag:
        snapshot = "{{data_interval_start | to_ds_month}}"
        wikitext_sensor = URLSensor(
            task_id="wait_for_data_in_mw_wikitext_history",
            url=f"{hadoop_name_node}/wmf/data/wmf/mediawiki/wikitext/history/snapshot={snapshot}/_PARTITIONED",
            poke_interval=timedelta(hours=1).total_seconds(),
        )
        revision_sensor = URLSensor(
            task_id="wait_for_data_in_raw_mediawiki_revision",
            url=f"{hadoop_name_node}/wmf/data/raw/mediawiki/tables/revision/snapshot={snapshot}/_PARTITIONED",
            poke_interval=timedelta(hours=1).total_seconds(),
        )

        # Usage: backfill_merge_into.py --backfill_source_table source_table visibility_source_table visibility_table
        #                               --target_table target_table --snapshot snapshot --wiki_dbs wiki0 wiki1
        #                               --years 2021-01
        # We can also add the --exclusive flag if we should NOT IN the wiki_dbs
        common_args = [
            "--backfill_source_table",
            props.hive_mediawiki_wikitext_history_table,
            "--visibility_source_table",
            props.hive_mediawiki_revision_table,
            "--target_table",
            props.hive_wikitext_raw_table,
            "--snapshot",
            snapshot,
            "--wiki_dbs",
            " ".join(group["members"]),  # type: ignore[arg-type]
        ]
        if group["exclusive"]:
            common_args.insert(8, "--exclusive")  # this flag must go before any list flag

        years = range(2001, props.current_year + 1)  # generate years since beginning of wiki time to now

        # generate one MERGE INTO per year per group for now
        merge_intos = []
        for year in years:
            merge_into = SparkSubmitOperator.for_virtualenv(
                task_id=f"spark_backfill_merge_into_{year}",
                virtualenv_archive=props.conda_env,
                entry_point="bin/backfill_merge_into.py",
                driver_memory=props.driver_memory,
                driver_cores=props.driver_cores,
                executor_memory=props.executor_memory,
                executor_cores=props.executor_cores,
                num_executors=props.num_executors,
                conf={
                    "spark.executor.memoryOverhead": props.spark_executor_memoryOverhead,
                    "spark.sql.shuffle.partitions": props.spark_sql_shuffle_partitions,
                    "spark.shuffle.io.retryWait": props.spark_shuffle_io_retryWait,
                    "spark.shuffle.io.maxRetries": props.spark_shuffle_io_maxRetries,
                    "spark.network.timeout": props.spark_network_timeout,
                    "spark.driver.maxResultSize": props.spark_driver_maxResultSize,
                    "spark.shuffle.file.buffer": props.spark_shuffle_file_buffer,
                    "spark.shuffle.service.index.cache.size": props.spark_shuffle_service_index_cache_size,
                    "spark.io.compression.lz4.blockSize": props.spark_io_compression_lz4_blockSize,
                    "spark.shuffle.service.enabled": props.spark_shuffle_service_enabled,
                    "spark.dynamicAllocation.enabled": props.spark_dynamicAllocation_enabled,
                    "spark.sql.adaptive.coalescePartitions.enabled": props.spark_sql_adaptive_coalescePartitions_enabled,  # noqa
                    "spark.jars.packages": "org.apache.iceberg:iceberg-spark-runtime-3.3_2.12:1.2.1",
                    "spark.driver.extraJavaOptions": "-Divy.cache.dir=/tmp/ivy_spark3/cache -Divy.home=/tmp/ivy_spark3/home",  # fix jar pulling  # noqa
                    "spark.jars.ivySettings": "/etc/maven/ivysettings.xml",  # fix jar pulling
                    "spark.yarn.archive": "hdfs:///user/xcollazo/artifacts/spark-3.3.2-assembly.zip",  # override 3.1's assembly  # noqa
                },
                launcher="skein",
                application_args=common_args + ["--years", year],
                use_virtualenv_spark=True,
                default_env_vars={
                    "SPARK_HOME": "venv/lib/python3.10/site-packages/pyspark",  # point to the packaged Spark
                    "SPARK_CONF_DIR": "/etc/spark3/conf",
                },
            )
            merge_intos.append(merge_into)

        wikitext_sensor >> revision_sensor >> merge_intos
