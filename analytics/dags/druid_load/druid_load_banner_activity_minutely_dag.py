"""
Extracts banner activity data from wmf.webrequest and loads it to Druid.

The loading is done with 2 DAGs: a daily one and a monthly one.
The daily DAG loads data to Druid as soon as it's available in Hive.
The monthly DAG loads a full month to Druid as one segment (more efficient).
If there were daily segments in Druid already, it will override them.
The monthly DAG also removes some dimension fields for privacy reasons.
It is executed with a 3 month delay, to match our data retention period.
Use the monthly DAG for back-filling and re-runs.
"""

from datetime import datetime, timedelta

from delayed_timetables import DelayedMonthlyTimetable

from analytics.config.dag_config import (
    alerts_email,
    artifact,
    create_easy_dag,
    dataset,
    hadoop_name_node,
    hql_directory,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.druid import HiveToDruidOperator
from wmf_airflow_common.operators.skein import SimpleSkeinOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator

props = DagProperties(
    # DAG start dates.
    daily_start_date=datetime(2023, 5, 15),
    monthly_start_date=datetime(2023, 3, 1),
    # Source data.
    webrequest_table="wmf.webrequest",
    # HQL query paths.
    druid_hql_directory=f"{hql_directory}/druid_load",
    daily_dag_hql="generate_daily_druid_banner_activity.hql",
    monthly_dag_hql="generate_daily_druid_banner_activity_aggregated_monthly.hql",
    # Intermediate (temporary) data.
    intermediate_database="tmp",
    intermediate_base_directory=f"{hadoop_name_node}/wmf/tmp/druid",
    # Time to wait until monthly recompaction and sanitization.
    # We can not wait the full 90 days (~31 of current month + 59 delay),
    # because at that point the source webrequest data will be already gone.
    # We execute the monthly DAG 10 days before that (59 - 10 = 49).
    monthly_dag_delay=timedelta(days=49),
    # Druid configs common to both DAGs.
    hive_to_druid_config={
        "druid_datasource": "banner_activity_minutely",
        "timestamp_column": "dt",
        "timestamp_format": "auto",
        "metrics": ["request_count", "normalized_request_count"],
        "query_granularity": "minute",
        "hadoop_queue": "production",
        "hive_to_druid_jar": artifact("refinery-job-0.2.14-shaded.jar"),
        # NOTE: for testing, set to a directory whose parent is owned by analytics-privatedata:druid
        "temp_directory": None,
    },
    # Configs for running very simple SparkSQL code from the Skein app.
    minimalist_spark_config={
        "master": "local",
        "driver_cores": 1,
        "driver_memory": "1G",
    },
    # SLAs and alerts email.
    daily_dag_sla=timedelta(hours=6),
    monthly_dag_sla=timedelta(hours=6),
    alerts_email=alerts_email,
)

default_tags = ["from_hive", "to_druid", "uses_hql", "uses_spark", "requires_wmf_webrequest"]

# Daily DAG.
with create_easy_dag(
    dag_id="druid_load_banner_activity_minutely_aggregated_daily",
    doc_md="Extracts and loads banner activity data from Hive to Druid daily.",
    start_date=props.daily_start_date,
    schedule="@daily",
    tags=["daily"] + default_tags,
    sla=props.daily_dag_sla,
    email=props.alerts_email,
) as daily_dag:
    # Table and directory where to store the temporary data to be loaded to Druid.
    intermediate_table = "tmp_{{dag.dag_id}}_{{data_interval_start | to_ds_nodash}}"
    intermediate_directory = (
        props.intermediate_base_directory + "/{{dag.dag_id}}/{{data_interval_start | to_ds_nodash}}"
    )

    sensor = dataset("hive_wmf_webrequest_text").get_sensor_for(daily_dag)

    formatter = SparkSqlOperator(
        task_id="format_data_to_load",
        sql=f"{props.druid_hql_directory}/{props.daily_dag_hql}",
        query_parameters={
            "source_table": props.webrequest_table,
            "destination_table": f"{props.intermediate_database}.{intermediate_table}",
            "destination_directory": intermediate_directory,
            "coalesce_partitions": 1,
            "year": "{{data_interval_start.year}}",
            "month": "{{data_interval_start.month}}",
            "day": "{{data_interval_start.day}}",
        },
        driver_cores=2,
        driver_memory="4G",
        executor_cores=4,
        executor_memory="8G",
        conf={
            "spark.dynamicAllocation.maxExecutors": "16",
            "spark.executor.memoryOverhead": 2048,
        },
    )

    loader = HiveToDruidOperator(
        task_id="load_to_druid",
        database=props.intermediate_database,
        table=intermediate_table,
        since="{{data_interval_start | to_ds_hour}}",
        until="{{data_interval_end | to_ds_hour}}",
        dimensions=[
            "campaign",
            "banner",
            "project",
            "uselang",
            "bucket",
            "anonymous",
            "status_code",
            "country",
            "country_matches_geocode",
            "region",
            "device",
            "sample_rate",
        ],
        segment_granularity="day",
        reduce_memory="4096",
        num_shards=1,
        **props.hive_to_druid_config,
    )

    hive_cleaner = SparkSqlOperator(
        task_id="remove_temporary_table",
        sql=f"DROP TABLE IF EXISTS {props.intermediate_database}.{intermediate_table};",
        **props.minimalist_spark_config,
    )

    hdfs_cleaner = SimpleSkeinOperator(
        task_id="remove_temporary_directory",
        script=f"hdfs dfs -rm -r {intermediate_directory}",
    )

    sensor >> formatter >> loader >> [hive_cleaner, hdfs_cleaner]


# Monthly DAG.
with create_easy_dag(
    dag_id="druid_load_banner_activity_minutely_aggregated_monthly",
    doc_md="Extracts and loads sanitized banner activity data from Hive to Druid monthly.",
    start_date=props.monthly_start_date,
    schedule=DelayedMonthlyTimetable(props.monthly_dag_delay),
    tags=["monthly"] + default_tags,
    sla=props.monthly_dag_sla,
    email=props.alerts_email,
) as monthly_dag:
    # Table and directory where to store the temporary data to be loaded to Druid.
    intermediate_table = "tmp_{{dag.dag_id}}_{{data_interval_start|to_ds_month_nodash}}"
    intermediate_directory = (
        props.intermediate_base_directory + "/{{dag.dag_id}}/{{data_interval_start|to_ds_month_nodash}}"
    )

    sensor = dataset("hive_wmf_webrequest_text").get_sensor_for(monthly_dag)

    formatter = SparkSqlOperator(
        task_id="format_data_to_load",
        sql=f"{props.druid_hql_directory}/{props.monthly_dag_hql}",
        query_parameters={
            "source_table": props.webrequest_table,
            "destination_table": f"{props.intermediate_database}.{intermediate_table}",
            "destination_directory": intermediate_directory,
            "coalesce_partitions": 8,
            "year": "{{data_interval_start.year}}",
            "month": "{{data_interval_start.month}}",
        },
        driver_cores=2,
        driver_memory="4G",
        executor_cores=4,
        executor_memory="16G",
        conf={
            "spark.dynamicAllocation.maxExecutors": "32",
            "spark.executor.memoryOverhead": 3072,
        },
    )

    loader = HiveToDruidOperator(
        task_id="load_to_druid",
        database=props.intermediate_database,
        table=intermediate_table,
        since="{{data_interval_start | to_ds_hour}}",
        until="{{data_interval_end | to_ds_hour}}",
        dimensions=[
            "campaign",
            "banner",
            "project",
            "uselang",
            "bucket",
            "anonymous",
            "status_code",
            "country",
            "country_matches_geocode",
            "device",
            "sample_rate",
        ],
        segment_granularity="month",
        reduce_memory="8192",
        num_shards=8,
        **props.hive_to_druid_config,
    )

    hive_cleaner = SparkSqlOperator(
        task_id="remove_temporary_table",
        sql=f"DROP TABLE IF EXISTS {props.intermediate_database}.{intermediate_table};",
        **props.minimalist_spark_config,
    )

    hdfs_cleaner = SimpleSkeinOperator(
        task_id="remove_temporary_directory",
        script=f"hdfs dfs -rm -r {intermediate_directory}",
    )

    sensor >> formatter >> loader >> [hive_cleaner, hdfs_cleaner]
