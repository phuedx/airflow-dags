"""
Loads event.editattemptstep from Hive to Druid.

The loading is done with 2 DAGs: an hourly one and a daily one.
The hourly DAG loads data to Druid as soon as it's available in Hive.
The daily DAG waits for a full day of data to be available in Hive,
and loads it all to Druid as a single daily segment (more efficient).
If there were hourly segments in Druid already, it will override them.
Use the daily DAG for back-filling and re-runs.
"""

from datetime import datetime, timedelta

from airflow import DAG

from analytics.config.dag_config import (
    artifact,
    dataset,
    default_args,
    druid_default_conf,
)
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.druid import HiveToDruidOperator
from wmf_airflow_common.templates.time_filters import filters

dag_id = "druid_load_editattemptstep"
var_props = VariableProperties(f"{dag_id}_config")

# Common configuration for both the hourly and the daily DAGs.
hive_to_druid_config = {
    "database": "event",
    "table": "editattemptstep",
    "druid_datasource": var_props.get("druid_datasource", "event_editattemptstep"),
    "timestamp_column": "dt",
    "timestamp_format": "auto",
    "query_granularity": "minute",
    "num_shards": 1,
    "hadoop_queue": var_props.get("hadoop_queue", "production"),
    "hive_to_druid_jar": var_props.get("hive_to_druid_jar", artifact("refinery-job-0.2.11-shaded.jar")),
    "temp_directory": var_props.get("temp_directory", None),  # Override just for testing.
    "dimensions": [
        "event.is_oversample",
        "event.action",
        "event.editor_interface",
        "event.mw_version",
        "event.platform",
        "event.integration",
        "event.page_ns",
        "event.user_class",
        "event.bucket",
        "useragent.browser_family",
        "useragent.browser_major",
        "useragent.device_family",
        "useragent.is_bot",
        "useragent.os_family",
        "useragent.os_major",
        "wiki",
        "webhost",
    ],
}

default_tags = ["from_hive", "to_druid", "uses_spark", "requires_event_editattemptstep"]

# Hourly DAG.
with DAG(
    dag_id=f"{dag_id}_hourly",
    doc_md="Loads editattemptstep from Hive to Druid hourly.",
    start_date=var_props.get_datetime("hourly_start_date", datetime(2023, 1, 20, 0)),
    # Do not schedule the DAG for the last hour of the day.
    # Otherwise, it would run in parallel with the daily DAG,
    # and race conditions could occur, corrupting Druid segments.
    schedule="0 0-22 * * *",
    tags=["hourly"] + default_tags,
    user_defined_filters=filters,
    default_args={
        **var_props.get_merged("default_args", default_args),
        **druid_default_conf,  # type: ignore
        "sla": timedelta(hours=6),
    },
) as hourly_dag:
    sensor = dataset("hive_event_editattemptstep").get_sensor_for(hourly_dag)

    loader = HiveToDruidOperator(
        task_id="load_to_druid",
        since="{{data_interval_start | to_ds_hour}}",
        until="{{data_interval_start | add_hours(1) | to_ds_hour}}",
        segment_granularity="hour",
        reduce_memory=4096,
        **hive_to_druid_config,
    )

    sensor >> loader


# Daily DAG.
with DAG(
    dag_id=f"{dag_id}_daily",
    doc_md="Loads editattemptstep from Hive to Druid daily.",
    start_date=var_props.get_datetime("daily_start_date", datetime(2022, 12, 20)),
    schedule="@daily",
    tags=["daily"] + default_tags,
    user_defined_filters=filters,
    default_args={
        **var_props.get_merged("default_args", default_args),
        **druid_default_conf,  # type: ignore
        "sla": timedelta(hours=12),
    },
) as daily_dag:
    sensor = dataset("hive_event_editattemptstep").get_sensor_for(daily_dag)

    loader = HiveToDruidOperator(
        task_id="load_to_druid",
        since="{{data_interval_start | to_ds_hour}}",
        until="{{data_interval_start | add_days(1) | to_ds_hour}}",
        segment_granularity="day",
        reduce_memory=8192,
        **hive_to_druid_config,
    )

    sensor >> loader
